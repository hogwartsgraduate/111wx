import http from "../utils/httpTool";

// 进入页面商品收藏状态接口
export const requestCollect = (data) => http.get("/p/user/collection/isCollection", data);

// 点击收藏按钮
export const requestCollectStatus = (data) => http.post("/p/user/collection/addOrCancel", data);