import http from "../utils/httpTool";
// 商品详情数据
export const requestGoodsDetail = (data) => http.get("/prod/prodInfo", data);