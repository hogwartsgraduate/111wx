import http from '@/utils/httpTool'

export const login = (data) => http.post('/login', data)

// 首页————轮播 数据
export const getSwiper = (data) => http.get('/indexImgs',data)
// 首页————通知 数据 
export const getScrollData = (data) => http.get('/shop/notice/topNoticeList',data)
// 首页————每日上新 商城热卖 更多宝呗 标题接口
export const getprodTagList = (data) => http.get('/prod/tag/prodTagList',data)
// 首页————每日上新 数据题目   /prodList
export const getprodList = (data) => http.get('/prod/prodListByTagId',data)

// 首页————搜索数据 /search/searchProdPage?current=1&prodName=Apple&size=10&sort=0
export const getDataSearch = (data) => http.get(`/search/searchProdPage`,data)
//购物车数据
export const shopcarData = (data) => http.post('/p/shopCart/info',data)
//购物车选择数据  /p/shopCart/totalPay
export const shopcarSelectData = (data) => http.post('/p/shopCart/totalPay',data)
//购物车总数量  /p/shopCart/prodCount
export const shopcarCountData = () => http.get('/p/shopCart/prodCount')
//购物车改变数据 /p/shopCart/changeItem
export const shopcarChangeItemData = (data) => http.post('/p/shopCart/changeItem',data)
//购物车删除商品  /p/shopCart/deleteItem
export const shopcarDeleteData = (data) => http.delete('/p/shopCart/deleteItem',data)
//提交订单页面  /p/order/confirm
export const shopListSubmit = (data) => http.post('/p/order/confirm',data)

//收藏的数量
export const collectionData = (data) => http.get('/p/user/collection/count',data)

//收藏的数据
export const requireCollectList = (data) =>  http.get('/p/user/collection/prods?current=1&size=10',data)

// 商品详情 
export const requestGoodsDetail = (data) => http.get("/prod/prodInfo", data);

// 地址列表数据
export const requestAddressList = () => http.get('/p/address/list')

// 发送验证码
export const requestsendCode=()=>http.post('/p/sms/send');
// 新品推荐————页面数据
export const getNewData = (data) => http.get("/prod/lastedProdPage", data);
// 限时特惠————页面数据
export const getLimitTime = (data) => http.get("/prod/discountProdList", data);
// 每日疯抢————页面数据/prod/moreBuyProdList
export const getDailyGet = (data) => http.get("/prod/moreBuyProdList", data);
//最新公告————页面数据 /shop/notice/noticeList
export const getNoticeList = (data) => http.get(`/shop/notice/info/${data}`, {});


//关于权限/p/user/setUserInfo
export const SetUserInfo = (data) =>http. put("/p/user/setUserInfo",data );

//全部订单列表
export const requireAllOrderList = (data) => http.get('/p/myOrder/myOrder',data)

//待支付订单数量
export const requirepay = (data) => http.get('/p/myOrder/orderCount',data)

//删除订单
export const requiredel = (data) => http.put('/p/myOrder/cancel/'+data)
//提交订单--提交 /p/order/submit
export const requestSubmit=()=>http.post('/p/order/submit');

