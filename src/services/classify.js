// 商品分类 
import http from "../utils/httpTool";
// 分类商品左侧
export const classifyLeft = (data) => http.get("/category/categoryInfo", data);

// 分类商品右侧   /prod/pageProd?categoryId=85
export const classifyRight = (data) => http.get("/prod/pageProd", data);