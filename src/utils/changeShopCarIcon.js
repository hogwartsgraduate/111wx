import {shopcarCountData} from '@/services/user'
import Vue from 'vue'
const changeShopcarCountData =async()=>{
    const res = await shopcarCountData()
    uni.setTabBarBadge({
        index: 3,
        text: `${res}`
      })
}

Vue.prototype.changeShopcarCountData = changeShopcarCountData
export default changeShopcarCountData



