import Vue from 'vue'
import App from './App'
import toPage from "utils/toPage"
import "@/utils/changeShopCarIcon"


Vue.config.productionTip = false

App.mpType = 'app'
Vue.prototype.$toPage = toPage

// 新建实例
async function createCloud(){
  const o_cloud = new wx.cloud.Cloud({
    resourceEnv: 'min-gucof',
    traceUser: true,
  })
  await o_cloud.init()
  return o_cloud;
}
Vue.prototype.$cloud = createCloud();

const app = new Vue({
  ...App
})
app.$mount()
